
var disk_time = {type: "f", value: 0.0};


Disk = function(num_patch, radio){
    this.num_patch = num_patch;
    this.radio = radio;
    this.phase = 0;
    this.patch = new Array(num_patch);
    this.figure;// = new THREE.Mesh();
    this.geometry = new THREE.Geometry();
    this.id = disk_uniforms.length;
    this.length = 30.0;
    this.density = 1 / num_patch;
    this.width = 2;
    //Initialization
    
    //this.figure.rotation.z = this.phase;
    
    var m1 = new THREE.Matrix4;
    var m2 = new THREE.Matrix4;
    m2.makeTranslation(0,this.radio,0);
    
    this.uniform = {
        //Globals for all disks
        time : disk_time,
        //For each disk
        phase : {type: "f", value: 0.0},
        id : {type:"i", value: this.id},
        color: {type: "v3", value: new THREE.Vector3()},
        width: {type: "f", value: this.width},
        starts: {type: "fv1", value: []},//[0, 5, 15, 20]},
        values: {type: "fv1", value: []},//[0.2, 0.6, 0.3, 0.0]},
        numberSections: {type:"i", value: 50},
        selected: {type:"i", value: -1},
        flags: {type:"i", value: 0}
    };
    
    var attributes = {
        //For each vertex
        rad: {type: "f", value: []},
        pos: {type: "f", value: []}
    };
    
    this.uniform.color.value.set(Math.random(),Math.random(),Math.random());
    disk_uniforms.push(this.uniform);
    
    this.material = new THREE.ShaderMaterial( 
	{
        side:           THREE.DoubleSide,
        transparent:    true,
        opacity:        0.5,
	    uniforms:       this.uniform,
        attributes:     attributes,
		vertexShader:   document.getElementById( 'vertexShader'   ).textContent,
		fragmentShader: document.getElementById( 'fragmentShader' ).textContent
	}   );
    
    //this.material = new THREE.MeshBasicMaterial({
        ////color:Math.random()>0.5?0x800000:0x000080,
        ////blending: THREE.NormalBlending,
        ////transparent:true,
        ////opacity:1,
        ////depthWrite:false,
        //side:THREE.DoubleSide,
        //vertexColors: THREE.FaceColors
        ////vertexColors: THREE.VertexColors
    //});
  
    var diff = 2*Math.PI/this.num_patch;
    var rad = 0;
    var i;
    
    
    this.geometry.vertices.push(new THREE.Vector3(radio*Math.cos(rad), radio*Math.sin(rad), -this.width/2));
    this.geometry.vertices.push(new THREE.Vector3(radio*Math.cos(rad), radio*Math.sin(rad), +this.width/2));
    attributes.rad.value.push(rad);
    attributes.rad.value.push(rad);
    attributes.pos.value.push(0.0);
    attributes.pos.value.push(0.0);
    
    for(var i = 1; i <= this.num_patch;i++){
        rad+=diff;
        this.geometry.vertices.push(new THREE.Vector3(radio*Math.cos(rad), radio*Math.sin(rad), -this.width/2));
        this.geometry.vertices.push(new THREE.Vector3(radio*Math.cos(rad), radio*Math.sin(rad), +this.width/2));
        
        attributes.rad.value.push(rad);
        attributes.rad.value.push(rad);
        attributes.pos.value.push(this.density*i);
        attributes.pos.value.push(this.density*i);
        
        this.geometry.faces.push ( new THREE.Face3(i*2-2,i*2-1,i*2, new THREE.Vector3(Math.cos(rad), Math.sin(rad), 0)) );
        this.geometry.faces.push ( new THREE.Face3(i*2-1,i*2,i*2+1, new THREE.Vector3(Math.cos(rad), Math.sin(rad), 0)) );
        //this.geometry.normals.push( new THREE.Vector3(Math.cos(rad), Math.sin(rad), 0) );
        //this.geometry.normals.push( new THREE.Vector3(Math.cos(rad), Math.sin(rad), 0) );
        //this.geometry.normals.push( new THREE.Vector3(Math.cos(rad), Math.sin(rad), 0) );
    }
    this.geometry.faces.push( new THREE.Face3(i*2-2,i*2-1,0) );
    this.geometry.faces.push( new THREE.Face3(i*2-1,0,1) );
    
    
        attributes.rad.value.push(rad);
        attributes.rad.value.push(rad);
        //attributes.pos.value.push(rad);
        //attributes.pos.value.push(rad);
    /*
    for(var i = 0; i < this.num_patch;i++){
        //console.log("Added " + i + " patch");
        this.patch[i] = new Patch(radio*Math.PI*2/num_patch, 3, 0x0000ff*((1+i)%2)+0xff0000*(i%2));
        this.patch[i].figure.matrixAutoUpdate = false;
        this.patch[i].figure.rotationAutoUpdate = false;
        
        m1.makeRotationZ(rad);
        this.patch[i].figure.matrix.multiplyMatrices(m1, m2);
        rad+=diff;
        
        THREE.GeometryUtils.merge(this.geometry, this.patch[i].figure, 1);
        
        
        //this.figure.add(this.patch[i].figure);
    }*/
    this.figure = new THREE.Mesh(this.geometry, this.material);
    //delete this.geometry;
    //delete this.material;
    this.figure.name = "DISK";
}

Disk.prototype.setWidth = function(w){
	//for(var i=0; i < this.num_data; i++){
		//this.data[i].setWidth(w/*(0.75+Math.sin(i/10.0)/4)*/);
	//}
    w = w/2;
    var f=this.geometry.vertices.length;
    for(var i=0; i < f ; i++){
        this.geometry.vertices[i].z = this.geometry.vertices[i].z > 0? w : -w;
    }
    this.geometry.verticesNeedUpdate = true;
}

Disk.prototype.setPhase = function(p){
    this.phase = p;
}

Disk.prototype.addPhase = function(p){
    //this.phase += p;
    var m1 = new THREE.Matrix4;
    m1.makeRotationZ(p);
    this.figure.matrix.multiplyMatrices(this.figure.matrix, m1);
    //this.figure.geometry.rotation.z += p;
    //this.figure.geometry.verticesNeedUpdate = true;
    //this.figure.geometry.elementsNeedUpdate = true;
}


Disk.prototype.setLength = function(l){
    this.length = l;
    this.uniform.length.value = l;
}

Disk.prototype.setSection = function(ini, end, identifier, width){
    
    this.uniform.starts.value.push(ini);
    this.uniform.values.value.push(width);
/*
    this.ini = ini/this.length * this.num_data;
    this.end = end/this.length * this.num_data;
    console.log(this.ini +" "+this.end);
    for(i = Math.floor(this.ini); i < this.end; i++){
        this.data[i].identifier = identifier;
        //this.data[i].color = color;
        this.data[i].setWidth(width);
    }
*/
}

Disk.prototype.setLayer = function(sizes, values, flags){
    //TODO comprobaciones sizes.length==values.length; flags para poner separaciones; otro parametro que sea color de las separaciones, y si es undefined no las pones
    var start = 0;
    var length = 0;
    var sections = sizes.length;
    var pad;
        
    this.uniform.starts.value = [];
    this.uniform.values.value = [];

    for (i = 0; i < sizes.length; i++) {
        length += sizes[i];
    }
    //console.log("abajo esta la length");
    //console.log(length);
    
    if (flags & torusFlags.pad) // sections padded
    {
        this.uniform.flags.value = 1;
        sections *= 2;
        pad = 0.1/sizes.length/1.1;
        length *= 1.1;

        for (i = 0; i < sections; i += 2) {
            this.uniform.starts.value[i] = start;   // padding
            this.uniform.values.value[i] = 1.0;
            start += pad;
            this.uniform.starts.value[i+1] = start;   // actual section
            this.uniform.values.value[i+1] = values[i/2];
            start += sizes[i/2]/length;
        }
    }
    else
    {
        this.uniform.flags.value = 0;
        for (i = 0; i < sections; i++) {
            this.uniform.starts.value[i] = start;
            this.uniform.values.value[i] = values[i];
            start += sizes[i]/length;
        }
    }

    this.uniform.numberSections.value = sections;

    if (this.id == 0)
    {
        console.log("setLayer");
        console.log(this.uniform);
    }
}

Disk.prototype.selectChromosome = function(chr) {
   this.uniform.selected.value = chr-1; // chromosomes are named within [1-23]
   
} 
