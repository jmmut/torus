////////////////////////
// Variables Globales
////////////////////////
//De Three
var renderer;
var scene;
var camera;
var stats;
var keyboard;
//Shaders
var disk_uniforms = new Array();

//Variables de animacion
var autoSpin = false;
var animation = true;
var clicado = -1;
var x;
var y;
var phase_toro = 0; // camelcase //FIXME
var translate_x = 0;
var giro_x = 0;
var giro_y = 0;
var rTri;
var distancia = 10;
var lastTime = 0;
var disk;
var toro;
var iter = 0;
var resource;
var torusFlags = {
    pad: 1,
    normalize: 2
}

flags = torusFlags.pad;

function initScene(canvas) {
    //Renderer
    renderer = new THREE.WebGLRenderer();
    renderer.setClearColor(0x7070B0, 1);
    renderer.setSize(canvas.width, canvas.height);
    renderer.sortObjects = false;
    
    
    //Escena
    scene = new THREE.Scene();

    //Camara
    camera = new THREE.PerspectiveCamera(45, canvas.width / canvas.height, 0.1, 20000);
    camera.position.set(20, 10, 20);
    camera.lookAt(scene.position);
    scene.add(camera);
    
}


function drawScene() {
    //console.log("Render Scene");
    renderer.render(scene, camera);
    //composer.render();
}


function animate(elapsed) {
    
    stats.update();
    controls.update();
    console.log("Animando");
    rTri = elapsed / 1000.0;
    iter++;
    //rSquare += (75 * elapsed) / 1000.0; 
    disk_time.value += elapsed/ 1000.0;
    var m1 = new THREE.Matrix4;
    m1.makeRotationY(rTri);
    if(toro){
        if (autoSpin)
            toro.addPhase(0.005);
        //toro.figure.applyMatrix(m1);
        /*
        var disk_num = Math.floor(Math.random()*toro.disk.length);
        for(var i = 0; i < 1000; i++){
            var face_r = Math.floor((Math.random()*toro.disk[disk_num].figure.geometry.faces.length )/2);
            //face_r = 0;
            
            toro.disk[disk_num].geometry.faces[face_r*2].color.setRGB(Math.random(), Math.random(), Math.random());
            toro.disk[disk_num].geometry.faces[face_r*2+1].color.copy(toro.disk[disk_num].geometry.faces[face_r*2].color);
            toro.disk[disk_num].geometry.colorsNeedUpdate = true;
        }
        */
        //toro.disk[disk_num].geometry.vertices[face_r*2].color.setRGB(Math.random(), Math.random(), Math.random());
        //toro.disk[disk_num].geometry.vertices[face_r*2+1].color.copy(toro.disk[disk_num].geometry.faces[face_r*2].color);
        //toro.disk[disk_num].geometry.colorsNeedUpdate = true;
        
        
        //console.log(toro.disk[disk_num]);
        
        
        
        //toro.disk[disk_num].patch[face_r].geometry.faces[0].color.setRGB(Math.random(), Math.random(), Math.random());
        //toro.disk[disk_num].patch[face_r].figure.geometry.faces[1].color.setRGB(Math.random(), Math.random(), Math.random());
        //toro.disk[disk_num].patch[face_r].geometry.colorsNeedUpdate = true;

        //toro.disk[20].patch[25].geometry.colorsNeedUpdate = true;
        
        //console.log(toro.disk[disk_num].figure.geometry.faces[face_r]);
    }
    if (disk)
        disk.figure.applyMatrix(m1);
}

function mainLoop(){
    
    if( animation == true){
        var timeNow = new Date().getTime();
        var elapsed = timeNow - lastTime;
        animate(elapsed);
        lastTime = timeNow;
        
        requestAnimationFrame(mainLoop);
    }

    drawScene();

}

function mouseMove(event)
{
	if(clicado != -1){
		x_ = event.clientX;
		y_ = event.clientY;
		switch(clicado){
			case 0:	//Left
				//giro_x += x_-x;
				translate_x += (x_-x)/(1000/distancia);
				giro_y += y_-y;
				break;
			case 1:	//Middle
				phase_toro = y_-y;
				toro.addPhase(phase_toro/100);
				break;
			case 2:	//Right
				
				break;
		}
		x=x_;
		y=y_;
	}
	
	requestAnimFrame(drawScene);
}
function mouseDown(event)
{
//	alert("mouseDown");
	x = event.clientX;
	y = event.clientY;
	clicado = event.button;
	//switch(event.button){
		//case 0:	//Left
			
			//break;
		//case 1:	//Middle
		
			//break;
		//case 2:	//Right
			
			//break;
	//}
}

function mouseUp(event){
	clicado = -1;
}

function mouseWheel(event){
	var delta = event.detail? event.detail*(-120) : event.wheelDelta;
	if(delta<0)
		distancia+=distancia/10+0.1;
	else
		distancia-=distancia/10+0.1;
	requestAnimFrame(drawScene);
}

function iniMove(){
	animation = !animation;
	
	if(animation == true){
		lastTime = new Date().getTime();
		mainLoop();
	}
}

//function initVariables(){
	//var caras = 300;
	//var giros = 0;
	//var discos = 50;
	//var width_data = 2;
	//var radio_toro = 2;
	//var radio_seccion = 1;/*	var caras = 6;
	//var giros = 0;
	//var discos = 20;
	//var width_data = 2;
	//var radio_toro = 2;
	//var radio_seccion = 1.9;*/
    
    //toro = new Torus(discos,caras,radio_toro,radio_seccion, width_data);
    //for(var i = 0; i < toro.n_disk; i++){
		//toro.disk[i].setPhase(i*PI*2/(toro.n_disk)*giros/caras);
	//}
//}
var RES;
var chr = new Array();
function initToro(res){
    var caras = 30;
	var discos = 30;
	var width_data = 1;
	var radio_toro = 10;
	var radio_seccion = 5;

    toro = new Torus(discos,caras,radio_toro,radio_seccion, width_data);

    toro.viewChromosomes(res, flags);

    resource = res;
    scene.add(toro.figure)

    //console.log(toro.disk[0].uniform);    //undefined??
}

function initModels(){
    //Grid
    var tam_grid = 20;
    var tam_cell = 5;
    var grid_dist = 15;
    
    var gridXZ = new THREE.GridHelper(tam_grid, tam_cell);
	gridXZ.setColors( new THREE.Color(0x006600), new THREE.Color(0x006600) );
	gridXZ.position.set( tam_grid-grid_dist,0-grid_dist,tam_grid -grid_dist);
	scene.add(gridXZ);
	
	var gridXY = new THREE.GridHelper(tam_grid, tam_cell);
	gridXY.position.set( tam_grid-grid_dist,tam_grid-grid_dist,0 -grid_dist);
	gridXY.rotation.x = Math.PI/2;
	gridXY.setColors( new THREE.Color(0x000066), new THREE.Color(0x000066) );
	scene.add(gridXY);

	var gridYZ = new THREE.GridHelper(tam_grid, tam_cell);
	gridYZ.position.set( 0-grid_dist,tam_grid-grid_dist,tam_grid -grid_dist);
	gridYZ.rotation.z = Math.PI/2;
	gridYZ.setColors( new THREE.Color(0x660000), new THREE.Color(0x660000) );
	scene.add(gridYZ);

    //Textures
    //var cuboTextura = new THREE.ImageUtils.loadTexture("img/crate.gif");
    //console.log(cuboTextura);
  
    //Patch
    //var patch = new Patch(4,4,0xFF0000);
    //scene.add(patch.figure);
    
    //Triángulo
    /*
    var material = new THREE.MeshBasicMaterial({
        color:0xFFFFFF,
        side:THREE.DoubleSide
    });
    var trianguloGeometria = new THREE.Geometry();
    trianguloGeometria.vertices.push(new THREE.Vector3( 0.0,  1.0, 0.0));
    trianguloGeometria.vertices.push(new THREE.Vector3(-1.0, -1.0, 0.0));
    trianguloGeometria.vertices.push(new THREE.Vector3( 1.0, -1.0, 0.0));
    trianguloGeometria.faces.push(new THREE.Face3(0, 1, 2));

    var triangulo = new THREE.Mesh(trianguloGeometria, material);
    triangulo.position.set(-1.5, 0.0, -7.0);
    scene.add(triangulo);
    // */
    
    //Disk test
    //disk = new Disk(4,2);
    //scene.add(disk.figure);
    
    //Torus
    //toro = new Torus(40, 20, 10, 4, 1);
    //scene.add(toro.figure);
    var url = "http://ws-beta.bioinfo.cipf.es/cellbasebeta2/rest/v3/hsapiens/genomic/chromosome/all?of=json";
    //var url = "file:///home/josemi/documentos/three/torus/js/citobands.json";
    $.getJSON(url, function(res){initToro(res)});
    
    //Lateral Panel
    
    //Other things
    
}




function webGLStart() {

    var top_margin = 0;//105;
    //Configure canvas
    var canvas = document.getElementById("canvas");
    canvas.width = window.innerWidth ;
    canvas.height = window.innerHeight-top_margin;
    
    
    
    //Set Listeners
    var listeners_on = false;
    if(listeners_on){
        canvas.onmousemove = function(event){mouseMove(event)};
        canvas.onmousedown = function(event){mouseDown(event)};
        canvas.onmouseup = function(event){mouseUp(event)};
        //canvas.onmousewheel = function(event){mouseWheel(event)};

        var mousewheelevt=(/Firefox/i.test(navigator.userAgent))? "DOMMouseScroll" : "mousewheel" //FF doesn't recognize mousewheel as of FF3.x
        canvas.addEventListener(mousewheelevt, mouseWheel, false);

        /*if (canvas.attachEvent) //if IE (and Opera depending on user setting)
            canvas.attachEvent("on"+mousewheelevt, mouseWheel)
        else if (canvas.addEventListener) //WC3 browsers
            canvas.addEventListener(mousewheelevt, mouseWheel, false)
        */
    }
    
    stats = new Stats();
    stats.domElement.style.position = 'absolute';
    stats.domElement.style.top = '0px';
    canvas.appendChild( stats.domElement );
    //Scene
    initScene(canvas);
    initModels();
    
    //Windows events
    keyboard = new THREEx.KeyboardState();
    THREEx.WindowResize(canvas, camera);
	THREEx.FullScreen.bindKey({ charCode : 'm'.charCodeAt(0) });
    
    //Append scene to canvas
    canvas.appendChild(renderer.domElement);

    controls = new THREE.OrbitControls( camera, renderer.domElement );
    

}

function include(jsFilePath) {
	var js = document.createElement("script");
	var head = document.getElementsByTagName('head')[0];
   
	js.type = "text/javascript";
	js.src = jsFilePath;
	
	document.head.appendChild(js);
	return js;
}

function main() {
	//include("js/glMatrix-0.9.5.min.js");
	//include("js/webgl-utils.js");
	//include("js/matrix_util.js");
	//include("js/Data.js");
	//include("js/Disk.js");
	//js = include("js/torus.js");
	//js.onload = webGLStart;
    
    
    webGLStart();
    
    //Animate
    lastTime = new Date().getTime();
    mainLoop();
   // var intervalID = setInterval(mainLoop, 100);
}
