
function Patch(l, w, color_p){

    this.length = l;
    this.width = w;
    this.identifier = -1;
    
    this.material;
    this.geometry;
    this.figure;
    
    
    //Initialization
    this.material = new THREE.MeshBasicMaterial({
        color:color_p,
        side:THREE.DoubleSide
    });
    
    this.geometry = new THREE.Geometry();
    this.geometry.vertices = [
       new THREE.Vector3(-this.length/2, 0.0,-this.width/2),
       new THREE.Vector3(-this.length/2, 0.0, this.width/2),
       new THREE.Vector3( this.length/2, 0.0, this.width/2),
       new THREE.Vector3( this.length/2, 0.0,-this.width/2)
    ];
    this.geometry.faces.push( new THREE.Face3(0,1,2) );
    this.geometry.faces.push( new THREE.Face3(2,3,0) );
    
    this.geometry.faces[0].color = new THREE.Color(color_p);
    this.geometry.faces[1].color = new THREE.Color(color_p);
           

    //var geom = new THREE.IcosahedronGeometry(1,0);
    //var geom2 = new THREE.PlaneGeometry(1,1,1,1);
    
    this.figure = new THREE.Mesh(this.geometry, this.material);
    //delete this.geometry;
    //delete this.material;
    
    //this.figure = new THREE.Mesh(geom, this.material);
    //this.figure = new THREE.Mesh(geom2, this.material);
    this.figure.name = "PATCH";
    
}

Patch.prototype = {
    
    setWidth: function(w) {
        this.width = w;
        //this.geometry.vertices = [
           //-this.length/2, 0.0,-this.width/2,
           //-this.length/2, 0.0, this.width/2,
            //this.length/2, 0.0,-this.width/2,
            //this.length/2, 0.0, this.width/2
        //];
        
    }
    
}
