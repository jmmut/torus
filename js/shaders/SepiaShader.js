/**
 * @author alteredq / http://alteredqualia.com/
 *
 * Sepia tone shader
 * based on glfx.js sepia shader
 * https://github.com/evanw/glfx.js
 */

THREE.SepiaShader = {

	uniforms: {

		"tDiffuse": { type: "t", value: null },
		"amount":   { type: "f", value: 1.0 },
        "time":     { type: "f", value: 1.0 }

	},

	vertexShader: [

		"uniform float time;",
		"varying vec2 vUv;",
		"void main() {",

            "vec3 newPosition;", 
			"vUv = uv;",
            
            "newPosition.x = position.x;",
            "newPosition.y = position.y;",
            "newPosition.z = position.z;",
			"gl_Position = projectionMatrix * modelViewMatrix * vec4( newPosition, uv.x/5.0 );",

		"}"

	].join("\n"),

	fragmentShader: [

		"uniform float amount;",
		"uniform float time;",

		"uniform sampler2D tDiffuse;",

		"varying vec2 vUv;  \
                            \
		void main() {",

			"vec4 color = texture2D( tDiffuse, vUv );",
			"vec3 c = color.rgb;",
			"color.r = dot( c, vec3( 1.0 - 0.607 * amount * cos(time), 0.769 * amount * sin(time), 0.189 * amount * cos(time) ) );",
			//"color.g = dot( c, vec3( 0.349 * amount * sin(time), 1.0 - 0.314 * amount * cos(time), 0.168 * amount * cos(time) ) );",
			//"color.b = dot( c, vec3( 0.272 * amount * cos(time), 0.534 * amount * sin(time), 1.0 - 0.869 * amount * cos(time) ) );",

			"gl_FragColor = vec4( min( vec3( 1.0 ), color.rgb ), color.a );",

		"}"

	].join("\n")

};
