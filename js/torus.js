


//function Torus(n_disk, n_data, r, disk_r) {
    //this.n_disk = n_disk;
    //this.radio = r;
    //this.phase = 0;
    //this.disk = new Array(n_disk);
    //for(var i=0; i < n_disk; i++){
        //this.disk[i] = new Disk(n_data, disk_r); 
        //this.disk[i].setWidth((r-disk_r)*2*PI/n_disk*2.5);
    //}
//}

function Torus(n_disk, n_data, r, disk_r, w) {
    this.n_disk = n_disk;
    this.radio = r;
    this.phase = 0;
    this.figure = new THREE.Object3D();
    
    
	var diff = 2*Math.PI/this.n_disk;
    var rad = 0;
    
    var m1 = new THREE.Matrix4;
    var m2 = new THREE.Matrix4;
    m2.makeTranslation(r,0,0);
    
    this.disk = new Array(n_disk);

    for(var i=0; i < n_disk; i++){
        this.disk[i] = new Disk(n_data, disk_r);
        this.disk[i].setWidth((r-disk_r)*2*Math.PI/n_disk*w);
        this.disk[i].figure.matrixAutoUpdate = false;
        this.disk[i].figure.rotationAutoUpdate = false;
        
        
        m1.makeRotationY(rad);
        m1.multiply(m2);

        this.disk[i].figure.applyMatrix(m1);
        rad+=diff;
        
        this.figure.add(this.disk[i].figure);
    }
    this.figure.name = "TORO";
}

Torus.prototype.addPhase = function(p){
	
    for(var i=0; i < this.n_disk; i++){
		this.disk[i].addPhase(p);
	}
}


Torus.prototype.setPhase = function(p){
	
    for(var i=0; i < this.n_disk; i++){
		this.disk[i].setPhase(p);
	}
}

Torus.prototype.viewChromosomes = function(res, flags){
    console.log("viewchr:");
    console.log(res);
    var length = 0;
    var pos = 0;
    var maxGenes = 0;
    var maxGenesPerNt = 0;
    var numSections = 22;

    var sizes = [];
    var values = [];
   
    for(i = 0; i < res.result.result[0].chromosomes.length; i++){
    
        //console.log("Length " + i + " " + length);
        //length += res.result.result[0].chromosomes[i].size;
        chr[res.result.result[0].chromosomes[i].name] = new Object;
        chr[res.result.result[0].chromosomes[i].name].size = res.result.result[0].chromosomes[i].size;
        chr[res.result.result[0].chromosomes[i].name].numberGenes = res.result.result[0].chromosomes[i].numberGenes;
        if (maxGenes < res.result.result[0].chromosomes[i].numberGenes)
            maxGenes = res.result.result[0].chromosomes[i].numberGenes;

        //length += res.result.result[0].chromosomes[i].size;
    }
    /*
    for(i = 1; i <= numSections; i++){   // take only 22 chromosomes due to there are two 'Y'
        length += chr[i.toString()].size;
        if (maxGenes < chr[i.toString()].numberGenes)
            maxGenes = chr[i.toString()].numberGenes;
    }
        
    console.log("Length " + length);
    pad = 1/10;  // reserve 10% to separate chromosomes
    length *= 1.1;
    for(i = 1; i <= numSections; i++){
        chr[i.toString()].ini = pos;
        console.log("pos[" + i + "] = " + pos);
        pos += chr[i.toString()].size/length;
        pos += pad/numSections;
    }
    
    toro = new Torus(discos,caras,radio_toro,radio_seccion, width_data);
    //toro = new Torus(40, 20, 10, 4, 1);
    //*/
 /*
    for(var i = 0; i < toro.n_disk; i++){
        //toro.disk[i].setLength(length);
          
        for(j = 1; j <= numSections; j++){
            
            toro.disk[i].setSection(chr[j.toString()].ini, 0, j, 1);
            toro.disk[i].setSection(chr[j.toString()].ini + pad/numSections, 0, j, chr[j.toString()].numGenes/maxGenes);
        }
	}
    */
    for(i = 1; i <= numSections; i++){   // take only 22 chromosomes due to there are two 'Y'
        if (maxGenes < chr[i.toString()].numberGenes)
            maxGenes = chr[i.toString()].numberGenes;
        if (maxGenesPerNt < chr[i.toString()].numberGenes/chr[i.toString()].size)
            maxGenesPerNt = chr[i.toString()].numberGenes/chr[i.toString()].size;
    }

    for(i = 1; i <= numSections; i++){   // take only 22 chromosomes due to there are two 'Y'
        sizes[i-1] = chr[i.toString()].size;
        if (flags & torusFlags.normalize)
            values[i-1] = chr[i.toString()].numberGenes/(maxGenesPerNt*chr[i.toString()].size);
        else
            values[i-1] = chr[i.toString()].numberGenes/maxGenes;

    }

    for(var i = 0; i < toro.n_disk; i++){
       toro.disk[i].setLayer(sizes, values, flags);
    }

}


